/* iptotal.c
 *
 * Version 0.3 Dec 17, 2001
 *
 * iptotal - network bandwidth measurement
 * By Antoine Megens <webmaster@dingetje.homeip.net>
 *
 * Much of this code is based on ipband
 * By Andrew Nevynniy <anevynni@russelmetals.com>
 *
 * iptotal is an extremely simple packet sniffer which only task is to
 * count the total amount of bytes transfered via the IP protocol.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

/*
------------------------------------------------------------------------
Include Files
------------------------------------------------------------------------
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <pcap.h>
#include <arpa/inet.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/socket.h>			/* BSD AF_INET */
#include <signal.h>
#include <unistd.h>
#include <time.h>
#include <netinet/in.h>			
#include <linux/if.h>
#include <errno.h>
#include <asm/errno.h>			/* for EINVAL */

/*
------------------------------------------------------------------------
Defines
------------------------------------------------------------------------
*/

#define DUMP
#undef  DUMP

#define VERSION_STR "iptotal 0.3" 

#define TRUE 1
#define FALSE 0

#define U_CHAR unsigned char

/*  Length of saved packets  */
#define PLEN 68 

/*  Length of packet headers */
#define POFF_ETH  14
#define POFF_PPP   4
#define POFF_RAW   0


/*
------------------------------------------------------------------------
Type Definitions
------------------------------------------------------------------------
*/

/*  Packet structure used by pcap library  */
typedef struct {
	U_CHAR src[6];
	U_CHAR dst[6];
	U_CHAR ptype[2];     /*  ==0x800 if ip  */
	} eth_struct_t;

typedef struct {
	U_CHAR version[1];
	U_CHAR service[1];
	U_CHAR length[2];
	U_CHAR id[2];
	U_CHAR flag[2];
	U_CHAR ttl[1];
	U_CHAR prot[1];
	U_CHAR chksum[2];
	U_CHAR srcip[4];
	U_CHAR dstip[4];
	U_CHAR srcpt[2];
	U_CHAR dstpt[2];
	} ip_struct_t;


/*  Subnet detail data */
typedef struct {
	long       nbyte;
	/* These 2 are keys for deleting detail data for a given subnet */
	int	   subnet_src;
	int        subnet_dst;
} data_t;

/*
------------------------------------------------------------------------
Global variables
------------------------------------------------------------------------
*/

extern int errno;
extern char pcap_version[];

/*
------------------------------------------------------------------------
Module variables
------------------------------------------------------------------------
*/

int    isig_m        = 0;		/* Interupt flag for capture loop */
int    preload_m     = FALSE;		/* Subnets are preloaded flag */
int    debug_m       = 0;		/* Debug option */
char   *pcapdev_m    = NULL;		/* Device to listen to */
pcap_t *pcapfile_m   = NULL;
int    pcapoffset_m  = 0;		/* IP header offset */
char   *filtercmd_m  = NULL;		/* Pcap filter string */
int    mask_m	     = 24;		/* Network aggregation mask bits */
int    rcycle_m	     = 300;		/* Number of sec to print total bytes */
int    fork_m 	     = FALSE;		/* Fork flag */
char   *config_m     = NULL;		/* Config file name */
time_t started_m     = (time_t) 0;	/* Time when we started */
int    promisc_m     = FALSE;		/* Use promiscious mode? */


/*
------------------------------------------------------------------------
Local Function Prototypes
------------------------------------------------------------------------
*/

void print_usage ();
void print_datalink ();
void set_defaults();
void dump_options();
int  read_config (char *);
void read_options (int argc, char *argv[]);
void open_interface (int); 
int  get_packetoffset (int);
void ihandler (int);
void get_two_tok(char *, char **, char **); 
int  is_space(char);
char *find_nonspace (char *);
char *find_space (char *); 
int  strcmpi (char *, char *);
int  is_true_str (char *);
int  compare_bytes (const void *, const void *);
void str2ip (char *, int *, int *);

/*
------------------------------------------------------------------------
Main Function
------------------------------------------------------------------------
*/

int main (int argc, char *argv[]) {

        struct pcap_pkthdr pkthdr;  
	U_CHAR *raw_pkt_save  = NULL;
        U_CHAR *raw_pkt       = NULL;  
	fd_set rdfs; 
	int fd;
	int retval;
	pid_t pid;
	eth_struct_t *eth_pkt = NULL;
	ip_struct_t  *ip_pkt  = NULL;
	time_t t0, t1;
	long total=0;
	long intotal=0;
	long outtotal=0;
	char iptst[50],*ipa;
  	struct sockaddr_in *ipaddr;

	char *config_name_def = "/etc/iptotal.conf";

	/*  Record application start time */
	started_m = time(NULL);

	/*  Read default config file */
	config_m = strdup(config_name_def);
	read_config(config_m);

	/*  Read command line options (override config file) and interface */
	read_options (argc, argv); 

	/* Print all options for debug */
	if (debug_m) dump_options();

	/*  Check for interface  */
	if( 1 == (argc-optind) ) {
		pcapdev_m = strdup(argv[optind]);
	}
	if( !pcapdev_m) {
		print_usage();
		return(1);
	}
/* get the IP address of this interface */
	{
  struct ifconf ifc;
  struct ifreq *req;
  char *buf, *facename,  *ptr;
  int facefound, lastlen, len, sock;

  facename = pcapdev_m;
  
  /* Adapted from W. Richards Stevens' _UNIX_Network_Programming_ */
  /* V.1 (2nd Ed) pp. 434 -439                                    */
  /*                                                              */
  /* Make sure buffer is big enough to hold information on all    */
  /* interfaces.                                                  */
  sock = socket(PF_INET, SOCK_DGRAM, 0);
  lastlen = 0;
  len = 100 * sizeof(struct ifreq);
  for(;;) {

    if((buf = malloc(len)) == NULL) {
      perror("malloc");
      exit(EXIT_FAILURE);
    }
    ifc.ifc_buf = buf;
    ifc.ifc_len = len;

    if(ioctl(sock, SIOCGIFCONF, &ifc) < 0) {
      if(errno != EINVAL || lastlen != 0) {
	perror("ioctl:SIOCGIFCONF");
	exit(EXIT_FAILURE);
      }
    } else {
   
      if(ifc.ifc_len == lastlen)

	/* success */
	break;
      lastlen = ifc.ifc_len;
    }

    /* increment buffer */
    len += 10 * sizeof(struct ifreq);
    free(buf);
  }

  /* find the interface of interest */
  facefound = 0;
  for(ptr = buf; ptr < buf + ifc.ifc_len; ) {
    req = (struct ifreq *)ptr;

    /* break when you find the right interface */
    if(!strncmp(req->ifr_name, facename, IFNAMSIZ)) {
      facefound = 1;
      break; 
    } 
    ptr += sizeof(struct ifreq);
  }
  
  if(facefound) {
    ipaddr = (struct sockaddr_in *) &req->ifr_addr;
    if (debug_m==3)
    {
 	printf("The IP address associated with %s is %s\n", 
   		facename, inet_ntoa(ipaddr->sin_addr));
    }

  } else
    printf("Interface %s not found\n", facename);

	}

	/* Try to fork */
	if (fork_m) {
	   switch (pid = fork()) {
	     case 0:
		break;
	     case -1:
		printf("Couldn't run in background, exiting...\n");
		exit(1);
	     default:
		printf("Running in background...%d\n",(int) pid);
		exit(0);
	   }
	}

	/* Convert number of mask bits to mask in hex */
	if (mask_m == 32) { 		/* Ugly */
		mask_m = 0xffffffff;
	} else {
		mask_m = 0xffffffff >> mask_m;
		mask_m ^= 0xffffffff;
	}

	/*  Open pcap file  */
	open_interface(promisc_m);
 
        /*  Allocate room for saved raw packet  */
        raw_pkt_save = (U_CHAR *) malloc (PLEN);
	
	/* Print datalink type as returned by pcap */
	if (debug_m) print_datalink();

        /*  Install interupt handler */
        signal (SIGINT,    ihandler);   /*  intercepts  ^C           */
        signal (SIGTERM,   ihandler);   /*  intercepts  ^kill <PID>  */

	/*  Initialize info for select(). Using select as might 
	    add multiple interfaces later */
	FD_ZERO (&rdfs);
        fd = pcap_fileno(pcapfile_m);
        FD_SET (fd, &rdfs);

	/*  Record cycle start time */
	t0 = time(NULL);

        /*  Read packets until interupt signal */
        while (isig_m==0) {

        /*  Wait for packet on one of the interfaces  */
        retval = select (fd+1, &rdfs, NULL, NULL, NULL);

	/*  If user interupt caught during select() call, retval will
	    be <0.  By continuing we re-test isig_m which should now
	    be set by the interupt handler 
	 */
	if (retval<0) continue;

	/*  Read packet */
	raw_pkt = (U_CHAR *) pcap_next (pcapfile_m, &pkthdr);
        if (raw_pkt==NULL) continue; 

	/*  Skip this packet if ethernet and not ip  */
	if (pcapoffset_m==POFF_ETH) {
	   eth_pkt = (eth_struct_t *) raw_pkt;
	   if (! (eth_pkt->ptype[0]==8 && eth_pkt->ptype[1]==0) ) continue;
	}

	/*  Find pointer to ip packet  */
	ip_pkt = (ip_struct_t *) (raw_pkt + pcapoffset_m);

	/*  Dump packet contents if debugging */
	if (3==debug_m) {
	  unsigned int ibyte;
	  int iwidth;
	  printf ("Raw Packet Length %d\n", pkthdr.len);
 	  printf ("Captured   Length %d\n", pkthdr.caplen);
	  printf ("Captured bytes ...\n");
	  iwidth=0;
	  for (ibyte=0;ibyte<pkthdr.caplen;ibyte++) {
		printf (" %03d", raw_pkt[ibyte]);
		if (++iwidth==16) {
			printf ("\n");
			iwidth=0;
		}
	  }
 	  printf ("\n");
	}

	/*  Set ports to 0 if not UDP or TCP  */
	if ( ip_pkt->prot[0]!=0x11 && ip_pkt->prot[0]!=0x06 ) {
		if (ip_pkt->prot[0]==1) {
			memset (ip_pkt->dstpt, 0, 2);
		} else {
			memset (ip_pkt->srcpt, 0, 2);
			memset (ip_pkt->dstpt, 0, 2);
        	}
	}

	/*  Dump packet ip data if debugging */
	if (3==debug_m) {
	  printf ("*%03d.%03d.%03d.%03d -> %03d.%03d.%03d.%03d  %3d %5d %5d\n",
	  ip_pkt->srcip[0],ip_pkt->srcip[1],ip_pkt->srcip[2],ip_pkt->srcip[3],
	  ip_pkt->dstip[0],ip_pkt->dstip[1],ip_pkt->dstip[2],ip_pkt->dstip[3],
	  ip_pkt->prot[0],
	  ip_pkt->srcpt[0]*256+ip_pkt->srcpt[1],
	  ip_pkt->dstpt[0]*256+ip_pkt->dstpt[1]);
	}
	sprintf(iptst,"%d.%d.%d.%d",
			ip_pkt->srcip[0],
			ip_pkt->srcip[1],
			ip_pkt->srcip[2],
			ip_pkt->srcip[3]);

	ipa = inet_ntoa(ipaddr->sin_addr);
	retval=strcmp(iptst,ipa);
/*	printf("iptst %s <-> %s = %d\n",iptst,ipa,retval);
*/
	if (retval==0)
	{
		outtotal += pkthdr.len;
	}
	else
	{
		intotal += pkthdr.len;
	}

	total += pkthdr.len;

	t1 = time(NULL);
	if ( (int) difftime(t1,t0) >= rcycle_m) {
	   double div = 1024 * rcycle_m;
	   double dtotal = (double) total / div;
	   double din = (double) intotal / div;
	   double dout = (double) outtotal / div;
	   t0 = t1;
	   printf("Total: %5.2f kBytes/s In: %5.2f Kbytes/s Out: %5.2f Kbytes/s\n",
			dtotal, din, dout);
	   total =0; intotal=0; outtotal=0;
	   break;
	 }
	} 	/* end of main loop (isig_m == 0) */

	/*  Close files  */    
	pcap_close(pcapfile_m);

	/*  Clear error if breaking during pcap call  */
	errno = 0;
	exit(0);
}

/*
------------------------------------------------------------------------
Local Functions
------------------------------------------------------------------------
*/

void print_usage(void) {
   printf("\nUsage: iptotal [OPTIONS] interface\n");
   printf("  -c filename   -  Read configuration file. Default is ");
   printf(                   "/etc/iptotal.conf.\n");
   printf("  -C            -  Ignore configuration file\n");
   printf("  -d level      -  Debug level: 0 - no debuging; 1 - summary;\n");
   printf("                   3 - all packets captured.\n");
   printf("  -f filterstr  -  Use pcap filters (see tcpdump).\n");
   printf("  -F            -  Fork and run in background.\n");
   printf("  -h            -  Print this help.\n");
   printf("  -m maskbuts   -  Set number of network mask bits (1-32)\n");
   printf("                   for subnet traffic aggregation.\n");
   printf("                   Default is 24 (255.255.255.0).\n");
   printf("  -P            -  Use promiscuous mode on network interface.\n");
   printf("  -r            -  Reporting period - number of seconds, default 300\n");
   printf("  -v            -  Print version and exit.\n");
   printf("\nExample:\n");
   printf("  iptotal eth0 -r 120\n");
   printf("\tWill measure the ip network usage on eth0 and prints out a\n");
   printf("\tusage report after 2 minutes.\n\n");
}


/*  Read options from command line  */
void read_options (int argc, char *argv[]) {

     char optchar;
     while(-1 != (optchar=getopt(argc,argv,"a:Ab:c:Cd:Ff:hl:L:m:M:o:Pr:t:v")))
     {
		switch (optchar) {
		case '?':
			exit(1);
		/*  Read config file */
		case 'c':
			set_defaults();
			config_m = strdup(optarg);
			read_config(config_m);
			break;
		/* Ignore config file */
		case 'C' :
			set_defaults();
			break;
		/*  Debugging option  */
		case 'd':
			debug_m = atoi(optarg);
			break;
		/*  Do we fork?  */
		case 'F':
			fork_m = TRUE;
			break;
		/*  Get pcap filter string  */
		case 'f':
			filtercmd_m = strdup (optarg);
			break;
		/*  Print help  */
		case 'h':
			print_usage();
			exit(1);
			break;
		/*  Get number of subnet mask bits  */
		case 'm':
			mask_m = atoi(optarg);
			if( (mask_m < 0) || (mask_m > 32)) {
			   printf("ERROR: invalid number of mask bits\n");
			   exit(1);
			   }
			break;
		/* use promiscuous mode */
		case 'P':
			promisc_m = TRUE;
			break;
		/*  Get reporting period in seconds */
		case 'r':
			rcycle_m = atoi(optarg);
			if( rcycle_m < 5 ) {
			   printf("ERROR: reporting period cannot be less ");
			   printf("than 5 sec.\n");
			   exit(1);
			   }
			break;
		/* Print version */
		case 'v':
			printf ("%s (compiled %s)\n", VERSION_STR, __DATE__);
			printf ("libpcap version %s\n", pcap_version);
			exit(0);
		default:
			exit(1);
		}
	}
}


void open_interface (int promisc) {

	struct bpf_program  fcode;
	char   ebuf[PCAP_ERRBUF_SIZE];

	pcapfile_m = pcap_open_live(pcapdev_m, PLEN, promisc, 1000, ebuf);

	if (pcapfile_m==NULL) {
	printf("iptotal: Trouble opening <%s>, msg=\"%s\"\n", 
			pcapdev_m, ebuf);
	exit(1);
	}

	/*  Find IP header offset  */
	pcapoffset_m = get_packetoffset(pcap_datalink(pcapfile_m));
	
	/*  Apply user requested packet filter code */
	if (pcap_compile(pcapfile_m, &fcode, filtercmd_m, 0, 0) < 0)
		printf("compile: %s", pcap_geterr(pcapfile_m));
	if (pcap_setfilter(pcapfile_m, &fcode) < 0)
		printf("setfilter:  %s", pcap_geterr(pcapfile_m));

	/*  Problem with pcap_setfilter?  Sets error, unset here  */
	errno = 0;

}

/* Return IP header offset depending on the interface type */
int get_packetoffset (int DataLinkType) {

	int PacketOffset;
	
	switch (DataLinkType) {
		case DLT_EN10MB:
		case DLT_IEEE802:
			PacketOffset = POFF_ETH;
			break;
		case DLT_PPP:
			PacketOffset = POFF_PPP;
			break;
		case DLT_RAW:
			PacketOffset = POFF_RAW;
			break;
		/* For others we guess  */
		default:
			PacketOffset = 0;
		}

	return PacketOffset;
}

/* Prints datalink type */
void print_datalink() {

printf ("Interface (%s) ", pcapdev_m);
switch (pcap_datalink(pcapfile_m)) {
case DLT_EN10MB:      printf ("DataLinkType = %s\n", "DLT_EN10MB"); break;
case DLT_IEEE802:     printf ("DataLinkType = %s\n", "DLT_IEEE802"); break;
case DLT_SLIP:        printf ("DataLinkType = %s\n", "DLT_SLIP"); break;
case DLT_SLIP_BSDOS:  printf ("DataLinkType = %s\n", "DLT_SLIP_BSDOS"); break;
case DLT_PPP:         printf ("DataLinkType = %s\n", "DLT_PPP"); break;
case DLT_PPP_BSDOS:   printf ("DataLinkType = %s\n", "DLT_PPP_BSDOS"); break;
case DLT_FDDI:        printf ("DataLinkType = %s\n", "DLT_FDDI"); break;
case DLT_NULL:        printf ("DataLinkType = %s\n", "DLT_NULL"); break;
case DLT_RAW:         printf ("DataLinkType = %s\n", "DLT_RAW"); break;
case DLT_ATM_RFC1483: printf ("DataLinkType = %s\n", "DLT_ATM_RFC1483"); break;
default:              printf ("DataLinkType = %d\n", pcap_datalink(pcapfile_m));
}
printf("\n");
}


/* Print options in use for debug purposes */
void dump_options () {
	

printf("\n%s (compiled %s)\n", VERSION_STR, __DATE__);
printf("libpcap version %s\n", pcap_version);
printf("started %s",ctime(&started_m));
printf("\nOption values:\n");
printf("\tDebug level: %d\n",debug_m);
printf("\tPromiscuous mode: %s\n",promisc_m?"yes":"no");
printf("\tConfiguration file: %s\n",config_m);
printf("\tReporting peroid (sec): %d\n",rcycle_m);
printf("\tPcap filter string: %s\n",filtercmd_m);
printf("\tSubnet mask bits: %d\n",mask_m);
printf("\n");

}

/* Interupt handler (called when program recieves operating system signal */
void ihandler (int cursig) {

	/*  Set flag to terminate main() polling loop 
	 *  when excution reaches bottom  */
	isig_m = 1;

	/*  FLUSH BUFFERS  */
	fflush (stdout);

	/*  RE-INSTALL SIGNAL HANDLER  */
	signal (cursig, SIG_DFL);

	if (debug_m) {
	time_t seconds;
	time (&seconds);
	fprintf (stderr, "iptotal received signal number <%i> ", cursig);
	fprintf (stderr, "on %s",ctime(&seconds));
	}

}

/* Parse config file */
int read_config (char *filename) {
	FILE *fin = NULL;
	char buffer[512];
	char *str;
	char *key, *val;

	fin = fopen (filename, "r");
	if (NULL==fin)  return errno;

        while ( (str=fgets(buffer, 512, fin)) ) { 

	get_two_tok(str, &key, &val); 

	/*  Test for comment or empty line  */
	if (*key=='#' || *key=='\0') continue;

	/* Test for valid options */
	if (!strcmpi("debug",key)) {
		debug_m = atoi(val);

	} else if (!strcmpi("filter",key)) {
		filtercmd_m = strdup(val);	

	} else if (!strcmpi("fork",key)) {
		fork_m = is_true_str(val);	

	} else if (!strcmpi("interface",key)) {
		pcapdev_m = strdup(val);

	} else if (!strcmpi("promisc",key)) {
		promisc_m = is_true_str(val);

	} else if (!strcmpi("report",key)) {
		rcycle_m = atoi(val);
		if( rcycle_m < 5 ) {
		   printf("ERROR: reporting period cannot be less ");
		   printf("than 5 seconds\n");
		   exit(1);
		   }
	} else if (!strcmpi("maskbits",key)) {
		mask_m = atoi(val);
		if( (mask_m < 0) || (mask_m > 32)) {
		   printf("ERROR: invalid number of mask bits\n");
		   exit(1);
		   }
	} else {
		fprintf(stderr,"iptotal: Error reading config file. ");
		fprintf(stderr,"  Unrecognized option: \"%s\"", key);

	} /* End of test for options */

	} /* End of while () looping through config file */

	fclose(fin); 

	return 1;
}


/*  Set all options to default values */
#define FREE(P) if ((P)!=NULL) { free(P); (P)=NULL; } 
void set_defaults(void) {

	debug_m       = FALSE;  
	preload_m     = FALSE;
	mask_m	      = 24;
	rcycle_m      = 300;
	fork_m	      = FALSE;
	promisc_m     = FALSE;

	/* These were malloc'ed by strdup and can be freed */
	FREE(config_m);
	FREE(pcapdev_m);
	FREE(filtercmd_m); 
}

/*  Return pointers to first two space delimited tokens, 
    null terminating first token if necessary.
    If no first,second token then pointers point to '\0'
*/
void get_two_tok(char *str, char **tok1, char **tok2) {

	/*  Find start of first token  */
	str = find_nonspace(str);
	*tok1 = *tok2 = str;
	if (*str=='\0') return;

	/*  Find end of first token  */
	*tok2 = str = find_space (str);
	if (*str=='\0') return;

	/*  terminate first token  */
	*(str++) = '\0';

	/*  find second token   */
	*tok2 = find_nonspace(str);

	/*  Remove trailing space  */
	str = str + strlen(str) - 1;
	while (is_space(*str)) {
		str--;
	}
	*(++str) = '\0';
}
	
/*  Test for space *OR* equals sign 
 *  (to allow shell scripts lines like TERM=vt1000 to be used as config
 *  files
 *  */
int is_space(char c) {
	return c==' ' || c=='\t' || c=='\n' || c=='\r' || c=='=';
}

/*  Find first non-space char */
char *find_nonspace (char *str) {
	while (*str && is_space(*str)) str++;
	return str;
}

/*  Find first space char */
char *find_space (char *str) {
	while (*str && !is_space(*str)) str++;
	return str;
}

/*  Compare two strings ignoring case  */
int strcmpi (char *a, char *b) {
	int equal = 1;
	char c,d;
	while (equal && *a) {
		c = *a++;
		d = *b++;
		if ('a'<=c && c<='z') c += 'A' - 'a';
		if ('a'<=d && d<='z') d += 'A' - 'a';
		equal = (c==d);
	}
	if (equal) return 0;
	if (c<d)   return -1;
	return 1;
}

/*  Return true of string is yes, on, ok ignoring case  */
int is_true_str (char *str) {
	return
		(! strcmpi("yes",str)) ||
		(! strcmpi("true",str)) ||
		(! strcmpi("on",str)) ||
		(! strcmpi("ok",str));
}

/*  Convert strings like "138.99.201.5" or "137.99.26" to int ip address  */
void str2ip (char *ipstr, int *ipout, int *mask) {
	int ip[4];
	int n = sscanf (ipstr, "%d.%d.%d.%d", ip, ip+1, ip+2, ip+3);
	int i;
	*ipout = 0;
	for (i=0;i<4;i++) {
		*ipout = *ipout<<8;
		if (i<n) *ipout |= (ip[i] & 0xff);
	}
	*mask = 0xffffffff >> (8*n);

	/* for reasons unknown 0xffffffff >> 32 -> -1, so set to 0  */
	if (*mask==-1)  *mask=0;
}
